﻿using System;
using System.Collections.Generic;

namespace Bibliothek
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Willkommen in der Bibliothek!");

            List<string> books = new List<string>();

            while (true)
            {
                Console.WriteLine("Verfuegbare Buecher:");
                foreach (string book in books)
                {
                    Console.WriteLine(book);
                }

                Console.WriteLine();
                Console.WriteLine("Bitte einen Buch-Titel eingeben:");
                string bookTitle = Console.ReadLine();
                books.Add(bookTitle);
            }
        }
    }
}
